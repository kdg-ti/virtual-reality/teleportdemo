﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GazeCamera : MonoBehaviour
{
    private GameObject teleportObject;
    private GameObject xrRig;
    private const float gazeDistance = 100;

    private void Start() {
        xrRig = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, gazeDistance)) {
            Debug.Log("Gameobject object hit!");
            if (hit.transform.gameObject.tag.Equals("Teleport")) {
                Debug.Log("Teleport object hit!");
                teleportObject = hit.transform.gameObject;
                teleportObject.GetComponent<Light>().enabled = true;
            } else {
                if (teleportObject != null) {
                    teleportObject.GetComponent<Light>().enabled = false;
                    teleportObject = null;
                }
            }
        } else {
            if (teleportObject != null) {
                teleportObject.GetComponent<Light>().enabled = false;
                teleportObject = null;
            }
        }

        if (Google.XR.Cardboard.Api.IsTriggerPressed || Input.GetKey(KeyCode.Space)) {
            Debug.Log("Teleporting...");
            if (teleportObject != null) {
                //transform.parent.position = new Vector3(teleportObject.transform.position.x, transform.parent.position.y, teleportObject.transform.position.z);
                xrRig.transform.position = new Vector3(teleportObject.transform.position.x, xrRig.transform.position.y, teleportObject.transform.position.z);
                teleportObject.GetComponent<Light>().enabled = false;
                teleportObject = null;
            }
        }
    }

}
